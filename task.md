ТЕМА ПРАКТИЧНОЇ РОБОТИ: 
РОЗРОБКА ГРИ "КРЕСТИКИ-НОЛІКИ" З ВИКОРИСТАННЯМ АЛГОРИТМУ "MINIMAX" 

Мета роботи:
Покращити знання по темі «Алгоритми пошуку». Отримати навички в реалізації алгоритму"Minimax" і застосування цього алгоритму в простих системах штучного інтелекту при імітації ігрового процесу.
Завдання:
1. Розробка основної логіки гри:
    - Створити функцію ініціалізацію дошки для гри "Крестики-ноліки".
    - Реалізувати функцію визначенння черговість ходу.
    - Розробити функцію для перевірки можливих ходів (доступних позицій).
   - Реалізувати функцію перевірки поточного стану (наявності переможця або нічієї).
    - Створити функцію визначення переможця.

2. Алгоритм "Minimax":
    - Реалізувати функцію `minimax`, яка визначатиме оптимальний хід для комп'ютера.
    - Створити функцію `best_move`, яка визначатиме найкращий хід для комп'ютера за допомогою алгоритму "Minimax".

3. Головний цикл гри:
    - Запрограмувати головний цикл гри, де гравець буде вводити свій хід, а потім комп'ютер робитиме свій відповідний хід за допомогою алгоритму "Minimax".
    - Додати умови завершення гри (перемога гравця або комп'ютера, нічия).

4. Тестування гри:
    - Перевірити гру, ввівши різні комбінації ходів і переконатися, що програма коректно визначає переможця чи нічію.

5. Додаткові завдання (за бажанням):
    - Додати графічний інтерфейс.
    - Реалізувати можливість гри між двома гравцями людьми.
    - Розширити логіку гри для більшої складності (різні алгоритми пошуку оптимальних ходів).

Вимоги до здачі роботи:
- Правильно реалізована гра "Крестики-ноліки" з використанням алгоритму "Minimax".
- Програмний код повинен бути читабельним та добре коментованим.
- Звіт, що включає в себе опис реалізованих функцій, пояснення алгоритму "Minimax", результати тестування гри та висновки.