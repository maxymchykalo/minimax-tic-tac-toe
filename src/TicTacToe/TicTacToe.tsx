import { useCallback, useEffect, useMemo, useState } from "react"
import { Player } from "./types";
import { initializeBoard, getNextToMove, getWinner, getStatusMessage, getBestMove } from "./utils";
import { makeMove } from "./utils/makeMove";
import { Board } from "./Board";


export const TicTacToe = ({ size, userFigure: user, aiEnabled }: { size: number; userFigure: Player; aiEnabled?: boolean }) => {
    const [board, setBoard] = useState(() => initializeBoard(size));

    const nextToMove = useMemo(() => getNextToMove(board), [board]);

    const nextOptimalMove = useMemo(() => getBestMove(board, nextToMove).move, [board, nextToMove]);

    const handleMove = useCallback((index: number) => {
        if (getWinner(board) || board[index]) return;
        const newBoard = makeMove(board, index)
        setBoard(newBoard);
    }, [board])

    const [shouldHighlightNextOptimalMove, setShouldHighlightNextOptimalMove] = useState(false);

    useEffect(() => {
        if(aiEnabled) {
            const nextPlayer = getNextToMove(board);
    
            if (nextPlayer === user) return;
    
            const nextMove = getBestMove(board, nextPlayer).move;
            if (typeof nextMove === 'number') {
                handleMove(nextMove)
            }
        }
    }, [aiEnabled, board, handleMove, user])

    return (
        <>
            <div className="game-board">
                <Board
                    board={board}
                    onMove={handleMove}
                    shouldHighlight={index => shouldHighlightNextOptimalMove && nextOptimalMove === index}
                />
            </div>
            <div>{getStatusMessage(board)}</div>
            <button
                disabled={shouldHighlightNextOptimalMove}
                onClick={() => setShouldHighlightNextOptimalMove(!shouldHighlightNextOptimalMove)}>
                    Highlight Next Optimal Move
            </button>
        </>
    )
}