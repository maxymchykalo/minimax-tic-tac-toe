import { BoardVM } from "../types";

export const initializeBoard = (size: number): BoardVM => {
    const board = [];
    for (let i = 0; i < size * size; i++) {
        board.push(null);
    }
    return board;

}