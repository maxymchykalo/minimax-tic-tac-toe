// import { genericMinimax } from "../../../minimax";
import { evaluateScore } from "./evaluateScore";
import { getAvailableMoves } from "./getAvailableMoves";
import { isGameOver } from "./isGameOver";
import { BoardVM, Player } from "../../types";
import { makeMove } from "../makeMove";
import { minimax } from "../../../minimax";

const getOpponent = (player: Player) => player === 'x' ? 'o' : 'x';

const ticTacToeMinimax = (minimax<BoardVM, number, Player>).bind(null, {
    getOpponent,
    evaluate: evaluateScore,
    getPossibleMoves: getAvailableMoves,
    makeMove,
    gameOver: isGameOver,
});

export const getBestMove = (board: BoardVM, player: Player) => {
    const availableMoves = getAvailableMoves(board);
    const bestMove = availableMoves.reduce((bestMove, move) => {
        const newBoard = makeMove(board, move);
        const moveValue = ticTacToeMinimax(newBoard, player, false);
        if (moveValue > bestMove.value) {
            return { move, value: moveValue };
        }
        return bestMove;
    }, { move: null, value: -Infinity } as { move: number | null, value: number });

    return bestMove;
}