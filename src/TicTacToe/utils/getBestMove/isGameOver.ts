import { getGameStatus } from "../getGameStatus";
import { BoardVM } from "../../types";

export const isGameOver = (board: BoardVM) => {
    return getGameStatus(board) !== 'in progress';
}