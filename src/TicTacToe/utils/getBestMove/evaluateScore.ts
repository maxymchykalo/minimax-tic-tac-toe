import { getWinner } from "../getWinner";
import { BoardVM, Player } from "../../types";

export const evaluateScore = (board: BoardVM, player: Player) => {
    const winner = getWinner(board);
    if(winner === null) {
        return 0;
    } else if (winner === player) {
        return 1;
    } 
    return -1;
}