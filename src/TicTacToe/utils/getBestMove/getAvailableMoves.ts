import { BoardVM } from "../../types";

export const getAvailableMoves = (board: BoardVM) => {
    return board.map((value, index) => value === null ? index : null).filter((value) => value !== null) as number[];
}