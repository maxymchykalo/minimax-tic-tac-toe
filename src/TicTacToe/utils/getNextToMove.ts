import { BoardVM } from "../types";

export const getNextToMove = (board: BoardVM) => {
    const xCount = board.filter((value) => value === 'x').length;
    const oCount = board.filter((value) => value === 'o').length;
    return xCount === oCount ? 'x' : 'o';
}
