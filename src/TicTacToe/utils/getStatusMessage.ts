import { getGameStatus } from "./getGameStatus";
import { getNextToMove } from "./getNextToMove";
import { getWinner } from "./getWinner";
import { BoardValue } from "../types";

export const getStatusMessage = (board: BoardValue[]) => {
    const status = getGameStatus(board);
    const nextPlayer = getNextToMove(board);
    const winner = getWinner(board);

    if (status === 'has winner') {
        return `Winner: ${winner}`;
    } else if (status === 'in progress') {
        return `Next Player: ${nextPlayer}`;
    } else {
        return 'Draw';
    }
}