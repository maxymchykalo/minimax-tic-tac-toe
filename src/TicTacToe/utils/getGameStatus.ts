import { getWinner } from "./getWinner";
import { BoardVM } from "../types";

type GameStatus = 'tie' | 'in progress' | 'has winner';

export const getGameStatus = (board: BoardVM): GameStatus => {
    const winner = getWinner(board);
    if (winner) {
        return 'has winner';
    }
    if (board.every((value) => value !== null)) {
        return 'tie';
    }
    return 'in progress';
}
