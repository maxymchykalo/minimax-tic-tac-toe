import { getNextToMove } from "./getNextToMove";
import { BoardVM } from "../types";

export const makeMove = (board: BoardVM, move: number) => {
    const nextToMove = getNextToMove(board);
    const newBoard = [...board];
    newBoard[move] = nextToMove;
    return newBoard;
}