import { getWinningCombinations } from "./getWinningCombinations";
import { BoardVM, Player } from "../types";

export const getWinner = (board: BoardVM): Player | null => {
    const size = Math.sqrt(board.length);
    const winner = getWinningCombinations(size).find((combination) => {
        const firstValue = board[combination[0]];
        if (firstValue === null) {
            return false;
        }
        return combination.every((index) => board[index] === firstValue);
    });
    if (winner) {
        return board[winner[0]];
    }
    return null;
}