
export const getWinningCombinations = (boardSize: number) => {
    const winningCombinations: number[][] = [];
    const winningCombinationLength = boardSize;

    // horizontal combinations
    for (let i = 0; i < boardSize; i++) {
        const combination: number[] = [];
        for (let j = 0; j < winningCombinationLength; j++) {
            combination.push(i * boardSize + j);
        }
        winningCombinations.push(combination);
    }

    // vertical combinations
    for (let i = 0; i < boardSize; i++) {
        const combination: number[] = [];
        for (let j = 0; j < winningCombinationLength; j++) {
            combination.push(i + j * boardSize);
        }
        winningCombinations.push(combination);
    }

    // diagonal combinations
    for (let i = 0; i < boardSize - winningCombinationLength + 1; i++) {
        const combination: number[] = [];
        for (let j = 0; j < winningCombinationLength; j++) {
            combination.push(i + j * (boardSize + 1));
        }
        winningCombinations.push(combination);
    }

    // anti-diagonal combinations
    for (let i = winningCombinationLength - 1; i < boardSize; i++) {
        const combination: number[] = [];
        for (let j = 0; j < winningCombinationLength; j++) {
            combination.push(i + j * (boardSize - 1));
        }
        winningCombinations.push(combination);
    }

    return winningCombinations;
}