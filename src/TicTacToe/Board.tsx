import { BoardVM, BoardValue } from "./types";


type SquareProps = {
    value: BoardValue,
    handleClick: () => void,
    shouldHighlight: boolean
}

const Square = ({ value, handleClick, shouldHighlight = false }: SquareProps) => {
    return (
        <button className="square" style={shouldHighlight ? { backgroundColor: 'gray' } : {}} onClick={handleClick}>
            {value || '-'}
        </button>
    );
};

type BoardProps = {
    shouldHighlight: (index: number) => boolean;
    board: BoardVM;
    onMove: (index: number) => void
}

export const Board = ({ board, onMove, shouldHighlight }: BoardProps) => {
    const size = Math.sqrt(board.length);
    const renderedBoard = [];
    for (let i = 0; i < size; i++) {
        const row = [];
        for (let j = 0; j < size; j++) {
            const index = i * size + j;
            row.push(<Square key={index} value={board[index]} handleClick={() => onMove(index)} shouldHighlight={shouldHighlight(index)} />)
        }
        renderedBoard.push(<div key={`row-${i}`} className="board-row">{row}</div>)
    }
    return <div className="game-board">
        {renderedBoard}
    </div>;
}