export type BoardValue = 'x' | 'o' | null;

export type BoardVM = BoardValue[];

export type Player = 'x' | 'o';
