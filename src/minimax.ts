export type MinimaxOptions<TState, TMove, TPlayer> = {
    evaluate: (state: TState, player: TPlayer) => number;
    getOpponent: (player: TPlayer) => TPlayer;
    getPossibleMoves: (state: TState) => TMove[];
    makeMove: (state: TState, move: TMove, player: TPlayer) => TState;
    gameOver: (state: TState) => boolean;
}

export const minimax = <T, U, P>(options: MinimaxOptions<T, U, P>, state: T, player: P, maximizing = false): number => {
    // Base case: If the game has ended or reached a terminal state
    if (options.gameOver(state)) {
        return options.evaluate(state, player);
    }
    const availableMoves = options.getPossibleMoves(state);
    if (maximizing) {
        return availableMoves.reduce((bestScore, move) => {
            const newState = options.makeMove(state, move, player);
            const score = minimax(options, newState, player, false);
            return Math.max(bestScore, score);
        }, -Infinity);
    } else {
        return availableMoves.reduce((bestScore, move) => {
            const opponent = options.getOpponent(player);
            const newState = options.makeMove(state, move, opponent);
            const score = minimax(options, newState, player, true);
            return Math.min(bestScore, score);
        }, Infinity);
    }
}