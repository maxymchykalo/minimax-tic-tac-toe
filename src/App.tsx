import { useState } from 'react';
import './App.css'
import { TicTacToe } from './TicTacToe'


type GameState = {
  step: 'select-size';
} | {
  step: 'select-player';
  size: number;
} | {
  step: 'play-game';
  size: number;
  player: 'x' | 'o';
  aiEnabled?: boolean;
}


function App() {

  const handleResetClick = () => {
    setState({ step: 'select-size' });
  }

  const [state, setState] = useState<GameState>({ step: 'select-size' });

  const [aiEnabled, setAiEnabled] = useState(false);

  return (

    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
      <h1>Welcome to the Tic Tac Toe game</h1>
      {state.step === 'select-size' && (
        <div>
        <h2>Select the size of the board</h2>
        <div>
          <button onClick={() => setState({ size: 2, step: 'select-player' })}>2x2</button>
          <button onClick={() => setState({ size: 3, step: 'select-player' })}>3x3</button>
          <button onClick={() => setState({ size: 4, step: 'select-player' })}>4x4</button>
        </div>
        </div>
      )}
      {state.step === 'select-player' && (
        <div>
          <h2>Select your figure</h2>
          <div>
            <button onClick={() => setState({ ...state, step: 'play-game', player: 'x', aiEnabled })}>Play as X</button>
            <button onClick={() => setState({ ...state, step: 'play-game', player: 'o', aiEnabled })}>Play as O</button>
            <label>
              <input type="checkbox" checked={aiEnabled} onChange={() => setAiEnabled(!aiEnabled)} />
              Play With AI
            </label>
        </div>
        </div>
      )}
      {state.step === 'play-game' && (
        <div style={{
          display: 'flex',
          flexDirection: 'column',
        }}>
          <div className="game">
            <TicTacToe size={state.size} userFigure={state.player} aiEnabled={state.aiEnabled} />
          </div>
          <div className="game-info">
            <button onClick={handleResetClick}>Reset Game</button>
          </div>
        </div>
      )}
      
    </div>
  )
}

export default App
